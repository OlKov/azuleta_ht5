package tests;

import org.testng.annotations.Test;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class SmokeTests extends BaseTest {

    private static final long DEFAULT_WAITING_TIME = 90;

    @Test
    public void checkAddToCart() {
        getHomePage().waitForPageLoadComplete(DEFAULT_WAITING_TIME);
        getHomePage().waitForAjaxToComplete(DEFAULT_WAITING_TIME);
        getHomePage().isSearchFieldVisible();
        getHomePage().enterTextToSearchField("6012-00");
        getHomePage().waitForPageLoadComplete(DEFAULT_WAITING_TIME);
        getHomePage().waitForAjaxToComplete(DEFAULT_WAITING_TIME);
        assertTrue(getProductPage().isAddToCartButtonEnabled());
        getProductPage().clickAddToCartButton();
        getProductPage().waitVisibilityOfElement(DEFAULT_WAITING_TIME, getProductPage().getAddToCartPopupHeader());
        assertTrue(getProductPage().isAddToCartPopupVisible());
        getProductPage().isContinueShoppingButtonVisible();
        getProductPage().isContinueToCartButtonVisible();
        assertEquals(getProductPage().getAddToCartPopupHeaderText(), "Product successfully added to your shopping cart");
        getProductPage().clickContinueToCartButton();

        assertTrue(getShoppingCartPage().isEmailInputVisible());
        assertTrue(getShoppingCartPage().isFirstNameVisible());
        assertTrue(getShoppingCartPage().isLastNameVisible());

        assertTrue(getShoppingCartPage().isCitySelectVisible());
        assertTrue(getShoppingCartPage().isPhoneInputVisible());
        assertTrue(getShoppingCartPage().isSaveButtonVisible());

        getShoppingCartPage().inputPersonalTestData();

        getShoppingCartPage().citySelectClick();
        getHomePage().waitForPageLoadComplete(DEFAULT_WAITING_TIME);
        getHomePage().waitForAjaxToComplete(DEFAULT_WAITING_TIME);
        assertTrue(getShoppingCartPage().isCityInputVisible());
        getHomePage().waitForPageLoadComplete(DEFAULT_WAITING_TIME);
        getHomePage().waitForAjaxToComplete(DEFAULT_WAITING_TIME);
        getShoppingCartPage().cityInputData();
        getHomePage().waitForPageLoadComplete(DEFAULT_WAITING_TIME);
        getHomePage().waitForAjaxToComplete(DEFAULT_WAITING_TIME);
        assertTrue(getShoppingCartPage().isSelectedCityFieldVisible());
        getShoppingCartPage().selectedCityFieldClick();
        getShoppingCartPage().saveButtonClick();
        assertTrue(getShoppingCartPage().isAlertSucessEnabled());

        assertTrue(getShoppingCartPage().isPostSelectVisible());
        getHomePage().waitForPageLoadComplete(DEFAULT_WAITING_TIME);
        getHomePage().waitForAjaxToComplete(DEFAULT_WAITING_TIME);
        getShoppingCartPage().postSelectClick();
        getHomePage().waitForPageLoadComplete(DEFAULT_WAITING_TIME);
        getHomePage().waitForAjaxToComplete(DEFAULT_WAITING_TIME);
        assertTrue(getShoppingCartPage().isPostSearchInputVisible());
        getHomePage().waitForPageLoadComplete(DEFAULT_WAITING_TIME);
        getHomePage().waitForAjaxToComplete(DEFAULT_WAITING_TIME);
        getShoppingCartPage().inputPostID();
        getHomePage().waitForPageLoadComplete(DEFAULT_WAITING_TIME);
        getHomePage().waitForAjaxToComplete(DEFAULT_WAITING_TIME);
        assertTrue(getShoppingCartPage().isSelectedPostDeptFieldVisible());
        getShoppingCartPage().selectedPostDeptFieldClick();

        getHomePage().waitForPageLoadComplete(DEFAULT_WAITING_TIME);
        getHomePage().waitForAjaxToComplete(DEFAULT_WAITING_TIME);

        assertTrue(getShoppingCartPage().isPayBlockVisible());
        assertTrue(getShoppingCartPage().isPrePayLinkVisible());
        assertTrue(getShoppingCartPage().isCashLinkVisible());
        getShoppingCartPage().cashClick();
        assertTrue(getShoppingCartPage().isConfirmOrderButtonVisible());

    }

    @Test
    public void checkProductComparison() throws InterruptedException {

        getHomePage().waitForPageLoadComplete(DEFAULT_WAITING_TIME);
        getHomePage().waitForAjaxToComplete(DEFAULT_WAITING_TIME);

        getHomePage().isSearchFieldVisible();
        //firstItem
        getHomePage().enterTextToSearchField("7500-C");
        getHomePage().waitForPageLoadComplete(DEFAULT_WAITING_TIME);
        getHomePage().waitForAjaxToComplete(DEFAULT_WAITING_TIME);
        assertTrue(getProductsListToCompare().isFirstItemToCompareVisible());

        assertTrue(getProductPage().isCompareButtonVisible());
        assertTrue(getProductsComparisonComponents().isInCompareFormHasZeroElements());

        getProductsListToCompare().mouseToItem(getProductsListToCompare().getFirstItemToCompare());
        getHomePage().waitForPageLoadComplete(DEFAULT_WAITING_TIME);
        getHomePage().waitForAjaxToComplete(DEFAULT_WAITING_TIME);
        assertTrue(getProductsListToCompare().isAddToCompareButtonVisible());
        getProductsListToCompare().addToCompareButtonClick();
        getHomePage().waitForPageLoadComplete(DEFAULT_WAITING_TIME);
        getHomePage().waitForAjaxToComplete(DEFAULT_WAITING_TIME);
        assertTrue(getProductsComparisonComponents().isInCompareFormHasOneElements());

        //secondItem
        getHomePage().enterTextToSearchField("TCC-02");
        getHomePage().waitForPageLoadComplete(DEFAULT_WAITING_TIME);
        getHomePage().waitForAjaxToComplete(DEFAULT_WAITING_TIME);
        assertTrue(getProductsListToCompare().isSecondItemToCompareVisible());
        getProductsListToCompare().mouseToItem(getProductsListToCompare().getSecondItemToCompare());
        getHomePage().waitForPageLoadComplete(DEFAULT_WAITING_TIME);
        getHomePage().waitForAjaxToComplete(DEFAULT_WAITING_TIME);
        assertTrue(getProductsListToCompare().isAddToCompareButtonVisible());
        getProductsListToCompare().addToCompareButtonClick();
        getHomePage().waitForPageLoadComplete(DEFAULT_WAITING_TIME);
        getHomePage().waitForAjaxToComplete(DEFAULT_WAITING_TIME);
        assertTrue(getProductsComparisonComponents().isInCompareFormHasTwoElements());
        //thirdItem
        getHomePage().enterTextToSearchField("31281");
        getHomePage().waitForPageLoadComplete(DEFAULT_WAITING_TIME);
        getHomePage().waitForAjaxToComplete(DEFAULT_WAITING_TIME);
        assertTrue(getProductsListToCompare().isThirdItemToCompareVisible());
        getProductsListToCompare().mouseToItem(getProductsListToCompare().getThirdItemToCompare());
        getHomePage().waitForPageLoadComplete(DEFAULT_WAITING_TIME);
        getHomePage().waitForAjaxToComplete(DEFAULT_WAITING_TIME);
        assertTrue(getProductsListToCompare().isAddToCompareButtonVisible());
        getProductsListToCompare().addToCompareButtonClick();
        getHomePage().waitForPageLoadComplete(DEFAULT_WAITING_TIME);
        getHomePage().waitForAjaxToComplete(DEFAULT_WAITING_TIME);
        assertTrue(getProductsComparisonComponents().isInCompareFormHasThreeElements());
        getProductPage().compareButtonClick();
        getHomePage().waitForPageLoadComplete(DEFAULT_WAITING_TIME);
        getHomePage().waitForAjaxToComplete(DEFAULT_WAITING_TIME);
        //ProductsComparisonPage
        assertTrue(getProductsComparisonPage().isPageHeadingVisible());
        assertTrue(getProductsComparisonPage().isFirstItemToCompareVisible());
        assertTrue(getProductsComparisonPage().isSecondItemToCompareVisible());
        assertTrue(getProductsComparisonPage().isThirdItemToCompareVisible());
        assertTrue(getProductsComparisonPage().isRemoveButtonVisible());
        assertEquals(getProductsComparisonPage().countRemoveButtonOnPage(),3);
        assertTrue(getProductsComparisonPage().isAddToCartButtonEnabled());
        assertEquals(getProductPage().countAddToCartButtonOnPage(),3);
        assertTrue(getProductsComparisonPage().isViewButtonVisible());
        assertEquals(getProductsComparisonPage().countViewButtonOnPage(),3);
        assertTrue(getProductsComparisonPage().isBackButtonVisible());
        assertTrue(getProductsComparisonPage().isFeaturesVisible());

    }

    @Test
    public void checkMainComponentsOnHomePage() {
        getHomePage().waitForPageLoadComplete(DEFAULT_WAITING_TIME);
        getHomePage().waitForAjaxToComplete(DEFAULT_WAITING_TIME);

        assertTrue(getHomePage().isContactVisible());
        assertTrue(getHomePage().isLanguagesVisible());
        assertTrue(getHomePage().isCurrenciesVisible());

        assertTrue(getHomePage().isLogoVisible());
        assertTrue(getHomePage().isSearchFieldVisible());
        assertTrue(getHomePage().isSearchButtonVisible());
        assertTrue(getHomePage().isCartBlockVisible());
        assertTrue(getHomePage().isCartLinkVisible());
        assertTrue(getHomePage().isBlockTopMenuVisible());

        assertTrue(getHomePage().isYarnLinkVisible());
        assertTrue(getHomePage().isToolsLinkVisible());
        assertTrue(getHomePage().isAccessoriesLinkVisible());
        assertTrue(getHomePage().isBooksAndMagazinesLinkVisible());
        assertTrue(getHomePage().isDIYKitsLinkVisible());
        assertTrue(getHomePage().isAboutUsLinkVisible());
        assertTrue(getHomePage().isPaymentAndDeliveryLinkVisible());
        assertTrue(getHomePage().isSpaceLinkVisible());
        assertTrue(getHomePage().isWholesaleLinkVisible());
        assertTrue(getHomePage().isSliderRowVisible());
        assertTrue(getHomePage().isPopularLinkVisible());
        assertTrue(getHomePage().isBestSellerLinkVisible());
        assertTrue(getHomePage().isFooterVisible());
        assertTrue(getHomePage().isNewsletterInputVisible());
        assertTrue(getHomePage().isNewsletterButtonVisible());

        getHomePage().clickLanguages();
        assertTrue(getHomePage().isRussianLanguageEnabled());
        getHomePage().clickCurrencies();
        getHomePage().waitForAjaxToComplete(DEFAULT_WAITING_TIME);
        assertTrue(getHomePage().isUAHEnabled());

        getHomePage().clickCartButton();
        getHomePage().waitForPageLoadComplete(DEFAULT_WAITING_TIME);
        getHomePage().waitForAjaxToComplete(DEFAULT_WAITING_TIME);
        getHomePage().waitVisibilityOfElement(DEFAULT_WAITING_TIME, getShoppingCartPage().getShoppingCartTitle());
        assertTrue(getShoppingCartPage().isShoppingCartTitleVisible());
    }
}
