package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ShoppingCartPage extends BasePage{

    private final String EMAIL="test@ukr.net";
    private final String FIRST_NAME="FirstName";
    private final String LAST_NAME="LastName";
    private final String CITY="Киев";
    private final String PHONE="(066) 000-00-00";
    private final String POST_ID="335";


    @FindBy (xpath = "//h2[contains(text(),'Your shopping cart')]")
    private WebElement shoppingCartTitle;

    public boolean isShoppingCartTitleVisible() {
        return shoppingCartTitle.isDisplayed();
    }

    public WebElement getShoppingCartTitle() {
        return shoppingCartTitle;
    }

    @FindBy (xpath = "//div[@class='required text form-group']//input[@id='email']")
    private WebElement emailInput;
    public boolean isEmailInputVisible() {
        return emailInput.isDisplayed();
    }

    @FindBy (xpath = "//div[@class='required form-group']//input[@id='customer_firstname']")
    private WebElement firstNameInput;
    public boolean isFirstNameVisible() {
        return firstNameInput.isDisplayed();
    }

    @FindBy (xpath = "//div[@class='required form-group']//input[@id='customer_lastname']")
    private WebElement lastNameInput;
    public boolean isLastNameVisible() {
        return lastNameInput.isDisplayed();
    }

    @FindBy (xpath = "//div[@class='required text form-group']//span[@class='selection']")
    private WebElement citySelect;
    public boolean isCitySelectVisible() {
        return citySelect.isDisplayed();
    }
    public void citySelectClick(){
        citySelect.click();
    }

    @FindBy (xpath = "//input[contains(@class,'select2')]")
    private WebElement cityInput;
    public boolean isCityInputVisible(){
        return cityInput.isDisplayed();
    }
    public void cityInputClick(){
         cityInput.click();
    }
    public void cityInputData(){
        cityInput.sendKeys(CITY, Keys.ENTER);
    }

    @FindBy (xpath = "//span[contains(@class,'select2')]//li[contains(text(),'"+CITY+"')]")
    private WebElement selectedCityField;
    public boolean isSelectedCityFieldVisible(){
        return selectedCityField.isDisplayed();
    }
    public void selectedCityFieldClick(){
        selectedCityField.click();
    }


    @FindBy (xpath = "//div[@class='required form-group']//input[@id='phone_mobile']")
    private WebElement phoneInput;
    public boolean isPhoneInputVisible() {
        return phoneInput.isDisplayed();
    }

    @FindBy (xpath = "//button[@id='submitGuestAccount']")
    private WebElement saveButton;
    public boolean isSaveButtonVisible() {
        return saveButton.isDisplayed();
    }

    public void saveButtonClick(){
        saveButton.click();
    }

    public void inputPersonalTestData(){
        emailInput.sendKeys(EMAIL, Keys.ENTER);
        firstNameInput.sendKeys(FIRST_NAME,Keys.ENTER);
        lastNameInput.sendKeys(LAST_NAME, Keys.ENTER);

        phoneInput.sendKeys(PHONE, Keys.ENTER);
    }


    @FindBy (xpath = "//div[contains(@class,'alert-success')]")
    private WebElement alertMessage;
    public boolean isAlertSucessEnabled() {
        return alertMessage.isEnabled();
    }

    @FindBy (xpath = "//div[@class='delivery_options']//span[@class='selection']")
    private WebElement postSelect;
    public boolean isPostSelectVisible() {
        return postSelect.isDisplayed();
    }
    public void postSelectClick(){
        postSelect.click();
    }

    @FindBy (xpath = "//input[contains(@class,'select2')]")
    private WebElement postSearchInput;
    public boolean isPostSearchInputVisible() {
        return postSearchInput.isEnabled();
    }

    public void inputPostID(){
        postSearchInput.sendKeys(POST_ID,Keys.ENTER);
    }

    @FindBy (xpath = "//span[contains(@class,'select2')]//li[contains(text(),'"+POST_ID+"')]")
    private WebElement selectedPostDeptField;
    public boolean isSelectedPostDeptFieldVisible(){
        return selectedPostDeptField.isDisplayed();
    }
    public void selectedPostDeptFieldClick(){
        selectedPostDeptField.click();
    }


    @FindBy (xpath = "//div[contains (@class, 'opc-main-block') and contains (@id, 'payment_methods')]")
    private WebElement payBlock;

    public boolean isPayBlockVisible() {
        return payBlock.isDisplayed();
    }

    @FindBy (xpath = "//a[contains(text(),'Prepay')]")
    private WebElement prePayLink;

    public boolean isPrePayLinkVisible()
    {
        return prePayLink.isDisplayed();
    }

    @FindBy (xpath = "//a[@class='cash']")
    private WebElement cashLink;

    public boolean isCashLinkVisible() {
        return cashLink.isEnabled();
    }

    public void cashClick(){
        cashLink.click();
    }

    @FindBy (xpath = "//button[contains(@class,'button')]//span[contains(text(),'confirm')]")
    private WebElement confirmButton;

    public boolean isConfirmOrderButtonVisible() {
        return confirmButton.isEnabled();
    }

    public ShoppingCartPage(WebDriver driver) {
        super(driver);
    }
}
