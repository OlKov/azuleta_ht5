package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends BasePage {

    @FindBy(xpath = "//span[@class='shop-phone']")
    private WebElement contact;

    public boolean isContactVisible() {
        return contact.isDisplayed();
    }

    @FindBy(xpath = "//div[@class='languages-block']//div[contains (@class,'current')]")
    private WebElement languagesBlock;

    public boolean isLanguagesVisible() {
        return languagesBlock.isDisplayed();
    }

    public void clickLanguages() {
        languagesBlock.click();
    }

    @FindBy(xpath = "//div[@class='languages-block']//a[contains(@title, 'Русский')]")
    private WebElement russianLanguage;

    public boolean isRussianLanguageEnabled() {
        return russianLanguage.isEnabled();
    }

    @FindBy(xpath = "//div[@id='currencies-block-top']//div[contains(@class,'current')]")
    private WebElement currencies;

    public boolean isCurrenciesVisible() {
        return currencies.isDisplayed();
    }

    public void clickCurrencies() {
        currencies.click();
    }

    @FindBy(xpath = "//div[@id='currencies-block-top']//a[contains(text(), 'Гривна (UAH)')]")
    private WebElement UAH;

    public boolean isUAHEnabled() {
        return UAH.isEnabled();
    }

    @FindBy(xpath = "//div[contains(@id, 'header_logo')]")
    private WebElement logo;

    public boolean isLogoVisible() {
        return logo.isDisplayed();
    }

    @FindBy(xpath = "//input[contains(@class, 'search_query')]")
    private WebElement searchField;

    public boolean isSearchFieldVisible() {
        return searchField.isDisplayed();
    }

    @FindBy(xpath = "//button[contains(@class,'button-search')]")
    private WebElement searchButton;

    public boolean isSearchButtonVisible() {
        return searchButton.isDisplayed();
    }

    @FindBy(xpath = "//div[contains(@class, 'shopping_cart')]")
    private WebElement cartBlock;

    public boolean isCartBlockVisible() {
        return cartBlock.isDisplayed();
    }

    @FindBy(xpath = "//a[@title='View my shopping cart']")
    private WebElement cartLink;

    public boolean isCartLinkVisible() {
        return cartLink.isDisplayed();
    }

    public void clickCartButton() {
        cartLink.click();
    }

    @FindBy(xpath = "//div[contains(@id, 'block_top_menu')]")
    private WebElement blockTopMenu;

    public boolean isBlockTopMenuVisible() {
        return blockTopMenu.isDisplayed();
    }

    @FindBy(xpath = "//a[@title='Yarn' and @class='sf-with-ul']")
    private WebElement yarnLink;

    public boolean isYarnLinkVisible() {
        return yarnLink.isDisplayed();
    }

    @FindBy(xpath = "//a[@title='Tools' and @class='sf-with-ul']")
    private WebElement toolsLink;

    public boolean isToolsLinkVisible() {
        return toolsLink.isDisplayed();
    }

    @FindBy(xpath = "//a[@title='Accessories' and @class='sf-with-ul']")
    private WebElement accessoriesLink;

    public boolean isAccessoriesLinkVisible() {
        return accessoriesLink.isDisplayed();
    }

    @FindBy(xpath = "//a[@title='Books and magazines' and @class='sf-with-ul']")
    private WebElement booksAndMagazinesLink;

    public boolean isBooksAndMagazinesLinkVisible() {
        return booksAndMagazinesLink.isDisplayed();
    }

    @FindBy(xpath = "//a[@title='DIY Kits' and @class='sf-with-ul']")
    private WebElement DIYKitsLink;

    public boolean isDIYKitsLinkVisible() {
        return DIYKitsLink.isDisplayed();
    }

    @FindBy(xpath = "//a[@title='About us']")
    private WebElement aboutUsLink;

    public boolean isAboutUsLinkVisible() {
        return aboutUsLink.isDisplayed();
    }

    @FindBy(xpath = "//a[@title='Payment and delivery']")
    private WebElement paymentAndDeliveryLink;

    public boolean isPaymentAndDeliveryLinkVisible() {
        return paymentAndDeliveryLink.isDisplayed();
    }

    @FindBy(xpath = "//a[@title='Space']")
    private WebElement spaceLink;

    public boolean isSpaceLinkVisible() {
        return spaceLink.isDisplayed();
    }

    @FindBy(xpath = "//a[@title='Wholesale']")
    private WebElement wholesaleLink;

    public boolean isWholesaleLinkVisible() {
        return wholesaleLink.isDisplayed();
    }

    @FindBy(xpath = "//div[@id='slider_row']")
    private WebElement sliderRow;

    public boolean isSliderRowVisible() {
        return sliderRow.isDisplayed();
    }

    @FindBy(xpath = "//a[@class='homefeatured']")
    private WebElement popularLink;

    public boolean isPopularLinkVisible() {
        return popularLink.isDisplayed();
    }

    @FindBy(xpath = "//a[@class='blockbestsellers']")
    private WebElement bestSellerLink;

    public boolean isBestSellerLinkVisible() {
        return bestSellerLink.isDisplayed();
    }

    @FindBy(xpath = "//footer")
    private WebElement footer;

    public boolean isFooterVisible() {
        return footer.isDisplayed();
    }

    @FindBy(xpath = "//input[@id='newsletter-input']")
    private WebElement newsletterInput;

    public boolean isNewsletterInputVisible() {
        return newsletterInput.isDisplayed();
    }

    @FindBy(xpath = "//button[@name='submitNewsletter']")
    private WebElement newsletterButton;

    public boolean isNewsletterButtonVisible() {
        return newsletterButton.isDisplayed();
    }

    public void enterTextToSearchField(final String searchText) {
        searchField.clear();
        searchField.sendKeys(searchText, Keys.ENTER);
    }

    public HomePage(WebDriver driver) {
        super(driver);
    }
}
