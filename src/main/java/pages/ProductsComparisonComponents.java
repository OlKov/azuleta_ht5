package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ProductsComparisonComponents extends BasePage{

    private final String ZERO="0";
    private final String ONE="1";
    private final String TWO="2";
    private final String THREE="3";

    @FindBy(xpath = "//form[@class='compare-form']//strong[contains(text(),'"+ZERO+"')]")
    WebElement compareFormZeroELements;

    public boolean isInCompareFormHasZeroElements(){
        return compareFormZeroELements.isDisplayed();
    }

    @FindBy(xpath = "//form[@class='compare-form']//strong[contains(text(),'"+ONE+"')]")
    WebElement compareFormOneELements;

    public boolean isInCompareFormHasOneElements(){
        return compareFormOneELements.isDisplayed();
    }

    @FindBy(xpath = "//form[@class='compare-form']//strong[contains(text(),'"+TWO+"')]")
    WebElement compareFormTwoELements;

    public boolean isInCompareFormHasTwoElements(){
        return compareFormTwoELements.isDisplayed();
    }

    @FindBy(xpath = "//form[@class='compare-form']//strong[contains(text(),'"+THREE+"')]")
    WebElement compareFormThreeELements;

    public boolean isInCompareFormHasThreeElements(){
        return compareFormThreeELements.isDisplayed();
    }

    public ProductsComparisonComponents(WebDriver driver) {
        super(driver);
    }
}
