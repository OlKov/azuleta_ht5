package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

public class ProductsListToCompare extends BasePage {

    @FindBy(xpath = "//img[contains(@title, 'ChiaoGoo')]")
    private WebElement firstItemToCompare;

    public boolean isFirstItemToCompareVisible() {
        return firstItemToCompare.isDisplayed();
    }

    public WebElement getFirstItemToCompare(){
        return firstItemToCompare;
    }




    @FindBy(xpath = "//img[contains(@title, 'Tulip')]")
    private WebElement secondItemToCompare;

    public boolean isSecondItemToCompareVisible() {
        return secondItemToCompare.isDisplayed();
    }

    public WebElement getSecondItemToCompare(){
        return secondItemToCompare;
    }

    @FindBy(xpath = "//img[contains(@title, 'KnitPro')]")
    private WebElement thirdItemToCompare;

    public boolean isThirdItemToCompareVisible() {
        return thirdItemToCompare.isDisplayed();
    }

    public WebElement getThirdItemToCompare(){
        return thirdItemToCompare;
    }

    public void mouseToItem(WebElement itemToCompare){
        WebElement mouseToItem = itemToCompare;
        Actions actionProvider = new Actions(driver);
        actionProvider.moveToElement(mouseToItem).build().perform();
    }

    @FindBy(xpath = "//a[contains(@class, 'add_to_compare')]")
    private WebElement addToCompareButton;

    public boolean isAddToCompareButtonVisible() {
        return addToCompareButton.isDisplayed();
    }

    public void addToCompareButtonClick(){
        addToCompareButton.click();
    }

    public ProductsListToCompare(WebDriver driver) {
        super(driver);
    }
}
