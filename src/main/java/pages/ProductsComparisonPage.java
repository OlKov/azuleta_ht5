package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import java.util.List;

public class ProductsComparisonPage extends BasePage {

    @FindBy(xpath = "//h1[contains(text(),'Product Comparison')]")
    private WebElement pageHeading;

    public boolean isPageHeadingVisible() {
        return pageHeading.isDisplayed();
    }

    @FindBy(xpath = "//a[contains(@title, 'ChiaoGoo')]//img")
    private WebElement firstItemToCompare;

    public boolean isFirstItemToCompareVisible() {
        return firstItemToCompare.isDisplayed();
    }

    public WebElement getFirstItemToCompare(){
        return firstItemToCompare;
    }

    @FindBy(xpath = "//a[contains(@title, 'Tulip')]//img")
    private WebElement secondItemToCompare;

    public boolean isSecondItemToCompareVisible() {
        return secondItemToCompare.isDisplayed();
    }

    public WebElement getSecondItemToCompare(){
        return secondItemToCompare;
    }

    @FindBy(xpath = "//a[contains(@title, 'KnitPro')]//img")
    private WebElement thirdItemToCompare;

    public boolean isThirdItemToCompareVisible() {
        return thirdItemToCompare.isDisplayed();
    }

    public WebElement getThirdItemToCompare(){
        return thirdItemToCompare;
    }

    @FindBy(xpath = "//a[contains(@class,'remove')]")
    private WebElement removeButton;

    public boolean isRemoveButtonVisible() {
        return removeButton.isDisplayed();
    }

    @FindBy(xpath = "//a[contains(@class,'remove')]")
    private  List<WebElement> removeButtonList;

    public int countRemoveButtonOnPage() {
        List<WebElement> elementsList=removeButtonList;
        int amount = elementsList.size();
        return amount;
    }

    @FindBy(xpath = "//a[@title='View']")
    private WebElement viewButton;

    public boolean isViewButtonVisible() {
        return viewButton.isDisplayed();
    }

    @FindBy(xpath = "//a[@title='View']")
    private  List<WebElement> viewButtonList;

    public int countViewButtonOnPage() {
        List<WebElement> elementsList=viewButtonList;
        int amount = elementsList.size();
        return amount;
    }

    @FindBy(xpath = "//div[contains(@class, 'button-container')]//span[contains(text(), 'Add to cart')]")
    private WebElement addToCartButton;

    public boolean isAddToCartButtonEnabled() {
        return addToCartButton.isEnabled();
    }

    @FindBy(xpath = "//div[contains(@class, 'button-container')]//span[contains(text(), 'Add to cart')]")
    private List<WebElement> addToCartButtonList;

    public int countAddToCartButtonOnPage() {
        List<WebElement> elementsList=addToCartButtonList;
        int amount = elementsList.size();
        return amount;
    }

    @FindBy(xpath = "//ul[@class='footer_link']//a[contains(@class,'button')]")
    private WebElement backButton;

    public boolean isBackButtonVisible() {
        return backButton.isDisplayed();
    }

    @FindBy(xpath = "//span[contains(text(), 'Features')]")
    private WebElement textFeatures;
    @FindBy(xpath = "//td[contains(@class, 'feature-name')]")
    private WebElement rowFeatures;

    public boolean isFeaturesVisible(){
        return textFeatures.isDisplayed()&&rowFeatures.isDisplayed();
    }

    public ProductsComparisonPage(WebDriver driver) {
        super(driver);
    }
}
