package pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class ProductPage extends BasePage{

    @FindBy(xpath = "//a[contains(@class, 'add_to_cart')]")
    private WebElement addToCartButton;

    public boolean isAddToCartButtonEnabled() {
        return addToCartButton.isEnabled();
    }

    @FindBy(xpath = "//div[contains(@class, 'button-container')]//span[contains(text(), 'Add to cart')]")
    private List<WebElement> addToCartButtonList;

    public int countAddToCartButtonOnPage() {
        List<WebElement> elementsList=addToCartButtonList;
        int amount = elementsList.size();
        return amount;
    }

    @FindBy(xpath = "//button[contains(@class,'compare')]")
    private WebElement compareButton;

    public boolean isCompareButtonVisible() {
        return compareButton.isDisplayed();
    }

    public void compareButtonClick(){
        compareButton.click();
    }
    public void clickAddToCartButton() {
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", addToCartButton);
    }

    public WebElement getAddToCartButton() {
        return addToCartButton;
    }

    @FindBy(xpath = "//div[@id='layer_cart']//span[@class='title']")
    private WebElement addToCartPopupHeader;

    public String getAddToCartPopupHeaderText() {
        return addToCartPopupHeader.getText();
    }
    public WebElement getAddToCartPopupHeader() {
        return addToCartPopupHeader;
    }
    public boolean isAddToCartPopupVisible() {
        return addToCartPopupHeader.isDisplayed();
    }

    @FindBy(xpath = "//div[@class='button-container']//span[contains (@class, 'continue')]")
    private WebElement continueShoppingButton;

    public void isContinueShoppingButtonVisible() {
        continueShoppingButton.isDisplayed();
    }

    @FindBy(xpath = "//div[@class='button-container']//a[contains (@title, 'checkout')]")
    private WebElement checkoutButton;

    public void isContinueToCartButtonVisible() {
        checkoutButton.isDisplayed();
    }

    public void clickContinueToCartButton() {
        checkoutButton.click();
    }

    public ProductPage(WebDriver driver) {
        super(driver);
    }
}
